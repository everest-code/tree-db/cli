from abc import ABC, abstractmethod
from typing import Tuple, Any, Optional, List, Union, Tuple
from tree_cli.error import HintException


class Parser(ABC):
    def __init__(self, command: str, args: list):
        self.cmd = command
        self.args = args

    @abstractmethod
    def _validate(self) -> Tuple[bool, Optional[str]]:
        raise NotImplementedError()

    @abstractmethod
    def _parse(self) -> Any:
        raise NotImplementedError()

    @abstractmethod
    def _count(self) -> Union[int, List[int]]:
        raise NotImplementedError()

    @abstractmethod
    def _hint_params(self) -> dict:
        raise NotImplementedError()

    def _build_params_hint(self) -> str:
        def wrapper(val: Tuple[str, Any]) -> str:
            k, v = val
            if isinstance(v, type):
                return f"[<{k}>: {v.__name__}]"
            else:
                return f"[<{k}>: {v}]"

        if len(self._hint_params()) == 0:
            return "NoParams"

        return " ".join(map(wrapper, self._hint_params().items()))

    def get(self) -> Any:
        count = self._count()
        ex = ValueError("Args count mismatch than expected")
        if isinstance(count, list):
            if len(self.args) not in count:
                raise HintException(ex, self._build_params_hint())
        else:
            if len(self.args) != count:
                raise HintException(ex, self._build_params_hint())

        valid, err = self._validate()
        if not valid:
            raise TypeError(err)

        return self._parse()

    def __repr__(self) -> str:
        return f'<{self.__class__.__name__}(cmd="{self.cmd}", args={self.args})>'

    def __str__(self) -> str:
        return repr(self)
