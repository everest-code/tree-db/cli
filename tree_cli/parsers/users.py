from tree_cli.parsers import Parser
from typing import Tuple, Optional, Any
import re


class UserParser(Parser):
    def _count(self) -> int:
        if self.cmd == "list":
            return 0
        elif self.cmd in ["get", "delete"]:
            return 1
        elif self.cmd in ["update", "create"]:
            return 3

    def _hint_params(self) -> dict:
        if self.cmd == "list":
            return {}
        elif self.cmd in ["get", "delete"]:
            return {"username": str}
        elif self.cmd == "update":
            return {"username": str, "field": str, "value": Any}
        elif self.cmd == "create":
            return {"username": str, "password": str, "can_write": bool}

    def _validate(self) -> Tuple[bool, Optional[str]]:
        if self.cmd in ["get", "delete", "update", "create"]:
            if re.match(r"^[a-z][a-z.\-_0-9]{3,15}$", self.args[0]) is None:
                return False, "username is not valid"

        if self.cmd == "update":
            if self.args[1] not in ["password", "can_write"]:
                return (
                    False,
                    'the field is not valid, valids are "password" and "can_write"',
                )
            elif self.args[1] == "can_write" and self.args[2] not in ["0", "1"]:
                return False, "can_write only accepts 0 or 1"

        if self.cmd == "create" and self.args[2] not in ["0", "1"]:
            return False, "write access only accepts 0 or 1"

        return True, None

    def _parse(self) -> Optional[dict]:
        if self.cmd == "list":
            return None

        data = dict(username=self.args[0])
        if self.cmd == "update":
            data["field"] = self.args[1]
            if data["field"] == "can_write":
                data["value"] = True if self.args[2] == "1" else False
            else:
                data["value"] = self.args[2]
        elif self.cmd == "create":
            data["password"] = self.args[1]
            data["can_write"] = True if self.args[2] == "1" else False

        return data
