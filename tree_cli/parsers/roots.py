from tree_cli.parsers import Parser
from typing import Tuple, Optional
import re


class RootParser(Parser):
    def _count(self) -> int:
        if self.cmd in ["get", "delete", "create"]:
            return 1
        elif self.cmd in ["update", "list"]:
            return 0

    def _hint_params(self) -> dict:
        if self.cmd in ["update", "list"]:
            return {}
        elif self.cmd in ["get", "delete", "create"]:
            return {"root": str}

    def _validate(self) -> Tuple[bool, Optional[str]]:
        if self.cmd == "update":
            return False, "update is not available for namesapace root"
        elif self.cmd != "list":
            if re.match(r"^[a-z][a-z.\-_0-9]{3,15}$", self.args[0]) is None:
                return False, "root name is not valid"

        return True, None

    def _parse(self) -> Optional[dict]:
        if self.cmd == "list":
            return None

        return {"root": self.args[0]}
