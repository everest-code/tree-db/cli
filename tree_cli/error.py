class HintException(Exception):
    """
    An exception with hint
    """

    def __init__(self, parent_ex: Exception, hint: str):
        self._ex = parent_ex
        self.hint = hint
        if isinstance(parent_ex.args, tuple):
            self.args = parent_ex.args + (hint,)
        else:
            self.args = (parent_ex.args, hint)

    def raise_parent(self):
        raise self._ex
