from typing import Tuple
from tree_cli.handlers import Handler
from tree_cli.models import Response, Error, OK, User, List


class UserHandler(Handler):
    def do_list(self) -> Response:
        _, res = self._do_request("u/", "GET")
        if not res["ok"]:
            return Error(**res["error"])

        wrapper = lambda u: User(**u)
        return List(map(wrapper, res["payload"]))

    def do_get(self) -> Response:
        _, res = self._do_request(f'u/{self.data["username"]}', "GET")
        if not res["ok"]:
            return Error(**res["error"])

        return User(**res["payload"])

    def do_update(self) -> Response:
        action = "write" if self.data["field"] == "can_write" else "password"
        url = f'u/{self.data["username"]}/{action}'

        _, res = self._do_request(url, "PUT", {"value": self.data["value"]})
        if not res["ok"]:
            return Error(**res["error"])

        return OK()

    def do_create(self) -> Response:
        data = {
            "user": self.data["username"],
            "password": self.data["password"],
            "writeAllowed": self.data["can_write"],
        }
        _, res = self._do_request("u/", "POST", data)

        if not res["ok"]:
            return Error(**res["error"])

        return User(**res["payload"])

    def do_delete(self) -> Response:
        _, res = self._do_request(f'u/{self.data["username"]}', "DELETE")
        if not res["ok"]:
            return Error(**res["error"])
        return OK()
