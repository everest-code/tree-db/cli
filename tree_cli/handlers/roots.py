from typing import Tuple
from tree_cli.handlers import Handler
from tree_cli.models import Response, OK, Error, Root, List


class RootHandler(Handler):
    def do_list(self) -> Response:
        _, res = self._do_request("r/", "GET")
        if not res["ok"]:
            return Error(**res["error"])

        wrapper = lambda u: Root(**u)
        return List(map(wrapper, res["payload"]))

    def do_create(self) -> Response:
        data = {
            "name": self.data["root"],
        }
        _, res = self._do_request("r/", "POST", data)

        if not res["ok"]:
            return Error(**res["error"])

        return Root(**res["payload"])

    def do_create(self) -> Response:
        data = {
            "name": self.data["root"],
        }
        _, res = self._do_request("r/", "POST", data)

        if not res["ok"]:
            return Error(**res["error"])

        return Root(**res["payload"])

    def do_get(self) -> Response:
        _, res = self._do_request(f'r/{self.data["root"]}', "GET")

        if not res["ok"]:
            return Error(**res["error"])

        return Root(**res["payload"])

    def do_delete(self) -> Response:
        _, res = self._do_request(f'r/{self.data["root"]}', "DELETE")

        if not res["ok"]:
            return Error(**res["error"])

        return OK()
