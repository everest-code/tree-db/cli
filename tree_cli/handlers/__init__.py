from abc import ABC, abstractmethod
from typing import Any, Dict, Literal, Tuple, Type, Union
from requests import get, post, put, delete
from json import dumps
from base64 import b64encode
from tree_cli.models import Response
import sys

http_methods = Union[Literal["POST"], Literal["PUT"], Literal["GET"], Literal["DELETE"]]


class Handler(ABC):
    def __init__(self, cmd: str, data: Dict, url: str):
        self.cmd = cmd
        self.data = data
        self.url = url.replace("tree", "http", 1)
        self.auth = None

    def set_authentication(self, user: str, password: str) -> None:
        self.auth = (user, password)

    @staticmethod
    def __get_action_method(method: http_methods) -> Type[get]:
        if method == "POST":
            return post
        elif method == "PUT":
            return put
        elif method == "DELETE":
            return delete
        return get

    def __get_headers(self) -> Dict:
        data = {"Content-Type": "application/json", "User-Agent": "Tree-CLI/1.0.0"}

        if self.auth is not None:
            auth = "%s:%s" % self.auth
            data["Authorization"] = "Basic " + b64encode(auth.encode()).decode()

        return data

    def _do_request(
        self, url: str, method: http_methods, payload: Any = None
    ) -> Tuple[int, Any]:
        action = self.__get_action_method(method)
        res = action(
            f"{self.url}/{url}",
            headers=self.__get_headers(),
            data=dumps(payload) if payload is not None else payload,
        )
        return res.status_code, res.json()

    def _exec(self) -> Response:
        def default():
            raise NotImplementedError()

        return getattr(self, f"do_{self.cmd}", default)()

    def __call__(self, *args: Any, **kwds: Any) -> int:
        payload = self._exec()
        if not payload.is_ok():
            sys.stderr.write("ERR: " + str(payload) + "\n")
            return 1

        sys.stdout.write(str(payload) + "\n")
        return 0
